
import PackageDescription

let package = Package(
    name: "perfect-server",
    dependencies: [
    .Package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", majorVersion: 2),
    .Package(url: "https://github.com/PerfectlySoft/Perfect-Mustache.git",majorVersion: 2),
    // .Package(url:"https://github.com/PerfectlySoft/Perfect-Mosquitto.git", majorVersion: 1),
    .Package(url: "https://github.com/SwiftORM/MySQL-StORM.git", majorVersion: 1),
    .Package(url: "https://github.com/krzyzanowskim/CryptoSwift.git", majorVersion: 0)
    ]
)

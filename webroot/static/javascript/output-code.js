//3-dim array for storing last index of additionals (for every main object, object copy, object additional)
var lastAdditionals = new Array(new Array(new Array()));
var lastObjects = new Array();
var objChilds = new Array()
var submitFlag = false;
//function, that called after addition of main object
var postMainAdd = function(obj_num, last_num) {
 // do nothing
}

function inputCheck() {
	return submitFlag;
}

function prepareSubmit() {
	
	submitFlag=true;
}

//obj_num - main index of object in all used types
//obj_current - current object copy of defined obj_num
//req_num - index of current object additional
function AddRow(obj_num, req_num, obj_current) {
	var contentContainer;
	if (typeof obj_current === 'undefined') {
		obj_current = 0;
		contentContainer = document.getElementById("dop_" + obj_num + "_" + req_num);
	}
	else {
		contentContainer = document.getElementById("dop_" + obj_num + "_" + req_num + "_" + obj_current);
	}

	var contentBody = contentContainer.lastChild;
	//contentBody should be TBODY
	if (contentBody.nodeName == "TABLE") {
		//we have optional input
		contentBody = contentContainer.lastChild.firstChild;
	}
	var contentToCopy = contentBody.childNodes;
	//checking presence of add-button
	var isButton=0;
	if (contentBody.lastChild.firstChild.firstChild.nodeName=="INPUT") {
		isButton=1;
	}
	//the number of childs with(out) add button
	var numChilds = contentBody.childNodes.length-isButton;

	var addsCounter = document.getElementById("adds_counter_" + obj_num+ "_" + req_num + "_" + obj_current);
	var currentIndex = addsCounter.getAttribute("value");
	addsCounter.setAttribute("value", ++currentIndex);
	numChilds = numChilds/currentIndex;
	for (i=0;i<numChilds; i++) {
		//create new table row for clonned element
		var newTableRow = document.createElement("tr");
		newTableRow.setAttribute("id", "newRow" + obj_num + obj_current + req_num + currentIndex + i);
		if (isButton==0) {
			contentBody.appendChild(newTableRow);
		}
		else {
			contentBody.insertBefore(newTableRow, contentBody.lastChild);
		}
		
		tableRowClone(contentBody.childNodes[i], newTableRow, currentIndex, obj_current);

	}
}

//obj_num - main index of object in all used types
//obj_current - current object copy of defined obj_num
//del_row — row to remove
//req_num - index of current object additional
function RemoveRow(obj_num, req_num, del_row, obj_current) {
	var contentContainer;
	if (typeof obj_current === 'undefined') {
		obj_current = 0;
		contentContainer = document.getElementById("dop_" + obj_num + "_" + req_num);
	}
	else {
		contentContainer = document.getElementById("dop_" + obj_num + "_" + req_num + "_" + obj_current);
	}

	var contentBody = contentContainer.lastChild;
	//contentBody should be TBODY
	if (contentBody.nodeName == "TABLE") {
		//we have optional input
		contentBody = contentContainer.lastChild.firstChild;
	}

	//checking presence of add-button
	var isButton=0;
	if (contentBody.lastChild.firstChild.firstChild.nodeName=="INPUT") {
		isButton=1;
	}
	//the number of childs with(out) add button
	var numChilds = contentBody.childNodes.length-isButton;

	var addsCounter = document.getElementById("adds_counter_" + obj_num+ "_" + req_num + "_" + obj_current);
	var currentIndex = parseInt(addsCounter.getAttribute("value"))+1;
	numChilds = numChilds/currentIndex;
	for (i=0;i<numChilds; i++) {
		//create new table row for clonned element
		var currentTableRow = document.getElementById("newRow" + obj_num + obj_current + req_num + del_row + i);
		contentBody.removeChild(currentTableRow);

	}
	//updating counter
	addsCounter.setAttribute("value", --currentIndex);
}


//copy table row (from, where, Index of input collection, corresponding object index)
//if objectCopyFlag we copying full object, not simply adding new additional
//returns cloned input element
function tableRowClone(clonedRow, newTableRow, currentIndex, objIndex, objectCopyFlag) {
	var clonedInput = clonedRow.lastChild.firstChild;
	//check element type
	if ((clonedInput.nodeName == "SELECT") || (clonedInput.nodeName == "INPUT")) {
		var firstTd = clonedRow.firstChild.cloneNode(true);
		newTableRow.appendChild(firstTd);
		var secTd = clonedRow.lastChild.cloneNode(true);
		var newInput = secTd.firstChild;
		var elementName = clonedInput.getAttribute("name");
		var clonedId =  clonedInput.getAttribute("id");
		var elementId = "";
		//how much dimensions in our input array
		var dimensions = 1;
		//what we will insert in onChange function (if set)
		var onChangeReplace = "";
		var lastCharIndex = elementName.indexOf("[");
		//check if we have two different inputs in one cell
		var secInput = clonedRow.lastChild.lastChild;
		var differentInputs = false;
		var secElName = secInput.getAttribute("name");
		var secElId =  secInput.getAttribute("id");
		var newSecInput = secTd.lastChild;
		var secCharIndex = secElName.indexOf("[");
		if (secInput != clonedInput) {
			differentInputs = true;
		}
		//check if we have 2-dim input
		if (lastCharIndex != elementName.lastIndexOf("[")) {
			dimensions=2;
			elementId = elementName.substr(0, lastCharIndex) + "_"+objIndex+"_"+currentIndex;
			elementName = elementName.substr(0, lastCharIndex)+"["+objIndex+"]"+"["+currentIndex+"]";
			onChangeReplace = objIndex+","+currentIndex;
			if (differentInputs) {
				secElId = secElName.substr(0, secCharIndex) + "_"+objIndex+"_"+currentIndex;
				secElName = secElName.substr(0, secCharIndex)+"["+objIndex+"]"+"["+currentIndex+"]";
			}
		}
		else {
			//simple 1-dim
			if (typeof objectCopyFlag === 'undefined') {
				//new additional
				elementId = elementName.substr(0, lastCharIndex) + "_"+currentIndex;
				elementName = elementName.substr(0, lastCharIndex)+"["+currentIndex+"]";
				onChangeReplace = currentIndex;
				if (differentInputs) {
					secElId = secElName.substr(0, secCharIndex) + "_"+currentIndex;
					secElName = secElName.substr(0, secCharIndex)+"["+currentIndex+"]";
				}
			}
			else {
				//new object
				elementId = elementName.substr(0, lastCharIndex) + "_"+objIndex;
				elementName = elementName.substr(0, lastCharIndex)+"["+objIndex+"]";
				if (differentInputs) {
					secElId = secElName.substr(0, secCharIndex) + "_"+objIndex;
					secElName = secElName.substr(0, secCharIndex)+"["+currentIndex+"]";
				}
				onChangeReplace = objIndex;
			}
		}
		newInput.setAttribute("name", elementName);
		newInput.setAttribute("id", elementId);
		if (differentInputs) {
			newSecInput.setAttribute("name", secElName);
			newSecInput.setAttribute("id", secElId);
		}
		newTableRow.appendChild(secTd);
		//if we have some associated action, change it's destination
		if (newInput.hasAttribute("onChange")) {
            newInput.setAttribute("onChange", newInput.getAttribute("onChange").replace("(0)", "("+objIndex+")"));
			newInput.setAttribute("onChange", newInput.getAttribute("onChange").replace("'"+clonedId+"'", "'"+elementId+"'"));
			if (differentInputs) {
				newSecInput.setAttribute("onChange", newSecInput.getAttribute("onChange").replace("'"+secInput.getAttribute("id")+"'", "'"+secElId+"'"));
			}
		}
	}
	else {
		//fallback for all other elements
		var strToCopy = contentBody.childNodes[i].innerHTML;
		newTableRow.innerHTML = strToCopy.replace("[0]\"", "["+currentIndex+"]\"");
	}
}

function AddMain(obj_num) {
	var contentContainer = document.getElementById("main_" + obj_num);
	var contentToCopy = contentContainer.childNodes;
	var numChilds;
	
	var objCounter = document.getElementById("obj_counter_" + obj_num);
	var currentIndex = objCounter.getAttribute("value");
	objCounter.setAttribute("value", ++currentIndex);
	if (obj_num in objChilds) {
		//load number of childs
		numChilds = objChilds[obj_num];
	}
	else {
		//the number of childs without add button
		numChilds = contentContainer.childNodes.length-1;
		//save number for future use
		objChilds[obj_num] = numChilds;
	}

	for (i=0;i<numChilds; i++) {
		//copy only meaningfull elements
		if (((contentContainer.childNodes[i].nodeName == "TABLE") || (contentContainer.childNodes[i].nodeName == "FIELDSET")) & (contentContainer.childNodes[i].childNodes.length>0)) {
			var newElement = contentContainer.childNodes[i].cloneNode(false);
			//if additional is optional, table is last of childs
			//if no — it's only one child (last too)
			var clonedTable = contentContainer.childNodes[i].lastChild;
			var newTable = clonedTable.cloneNode(false);
			newElement.appendChild(newTable);
			//how many rows we need to copy
			var numRows = clonedTable.childNodes.length;
			if (newElement.getAttribute("id")) {
				var elementId = newElement.getAttribute("id");
				//update element id
				newElement.setAttribute("id",elementId+"_"+currentIndex);
				//checking additional or not
				var splitId = elementId.split("_");
				if (splitId[0]=="dop") {
					//we have additional
					//is this additional optional?
					if (contentContainer.childNodes[i].nodeName == "FIELDSET") {
						//we have one more level of nesting
						var lastTable = newTable;
						clonedTable = clonedTable.lastChild;
						newTable.setAttribute("id", "dopo_"+splitId[1]+"_"+splitId[2]+"_"+currentIndex);
						newTable.setAttribute("class", "invisible");
						newTable = clonedTable.cloneNode(false);
						numRows = clonedTable.childNodes.length;
						lastTable.appendChild(newTable);
						newOptionalsButton = contentContainer.childNodes[i].firstChild.cloneNode(false);
						newOptionalsButton.setAttribute("id", "ds_button_"+splitId[1]+"_"+splitId[2]+"_"+currentIndex);
						newOptionalsButton.setAttribute("onClick", "ShowAdds("+splitId[1]+","+splitId[2]+","+currentIndex+")");
						newOptionalsButton.setAttribute("class", "");
						newElement.insertBefore(newOptionalsButton, lastTable);
						newHiddenMarker = document.createElement("input");
						newHiddenMarker.setAttribute("name", "obj_optionals["+splitId[1]+"]["+splitId[2]+"]["+currentIndex+"]");
						newHiddenMarker.setAttribute("id", "ds_hid_"+splitId[1]+"_"+splitId[2]+"_"+currentIndex);
						newHiddenMarker.setAttribute("type", "hidden");
						newHiddenMarker.setAttribute("value", "");
						newElement.insertBefore(newHiddenMarker, lastTable);

					}
					//Check if we have in last row "AddRow" button
					if (clonedTable.lastChild.childNodes.length == 1) {
						//do not copy it (will be copied manually)
						numRows = numRows-1;

					}

					//correct number of rows on base of previously copied additionals
					var addsCounter = document.getElementById("adds_counter_" + splitId[1] + "_" + splitId[2] + "_0");
					if (addsCounter) {
						var addsNumber = addsCounter.getAttribute("value");
						numRows = numRows/(parseInt(addsNumber)+1);
					}
				}
			}

			for (ir=0;ir<numRows; ir++) {
				var currentRow = clonedTable.childNodes[ir];

				var newTableRow = document.createElement("tr");
				newTable.appendChild(newTableRow);

				tableRowClone(currentRow, newTableRow, 0, currentIndex, true);
			}
			//Check if we have in last row "AddRow" button
			if (clonedTable.lastChild.childNodes.length == 1) {
				//manual copy of button 
				var newTableRow = document.createElement("tr");
				newTable.appendChild(newTableRow);
				var newTableCell = document.createElement("td");
				newTableRow.appendChild(newTableCell);
				var newButton = clonedTable.lastChild.firstChild.firstChild.cloneNode();
				newTableCell.appendChild(newButton);
				newButton.setAttribute("onClick", "AddRow("+splitId[1]+","+splitId[2]+","+currentIndex+")");
				//copy additionals counter
				var newCounter = addsCounter.cloneNode();
				newCounter.setAttribute("id", "adds_counter_"+splitId[1]+"_"+splitId[2]+"_"+currentIndex);
				newCounter.setAttribute("name", "adds_instances["+splitId[1]+"]["+splitId[2]+"]["+currentIndex+"]");
				newCounter.setAttribute("value", "0");
				contentContainer.insertBefore(newCounter,contentContainer.lastChild);

			}
			contentContainer.insertBefore(newElement, contentContainer.lastChild);
		}
		else {
			//it is simply empty table, so we do not interested in it
		}
	}
    ShowAdditionals(currentIndex)
	postMainAdd(obj_num, currentIndex);
}
function ShowAdds(obj_num, add_num, obj_current) {
	var obj_designation = obj_num + "_" + add_num + "_" + obj_current;
	var tek_element = document.getElementById("dopo_" + obj_designation);
	tek_element.setAttribute("class", "");
	var button_element = document.getElementById("ds_button_" + obj_designation);
	button_element.setAttribute("class", "invisible");
	var hid_element = document.getElementById("ds_hid_" + obj_designation);
	hid_element.setAttribute("value", "set");
}

function makeFloat(value) {
	return parseFloat(value.replace(",", "."));
}

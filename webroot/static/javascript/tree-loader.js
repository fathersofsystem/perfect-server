window.onload = OnLoadAction;
Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++)  {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}	
var tree_array = [
                  [1],
                  [0,1,2,7],
                  [3],
                  [4,5,6],
                  [10],
                  [0,4,6,8,9]
                  ];
var list_name;
var adds_type;
var adds_num;

function ShowAdditionals(id) {
	var selected_type = document.getElementById(list_name+"_"+id);
	var sel_val = selected_type.options[selected_type.selectedIndex].value;
    var id_in_array = selected_type.options[selected_type.selectedIndex].value - 1
	var type_array = tree_array[id_in_array.toString()];
	for(var di = 0; di < adds_num; di++)  {
        var dop_id = "dop_0_"+di;
        if (id>0) {
            dop_id += "_"+id
        }
		var tek_element = document.getElementById(dop_id);
		if (tek_element) {
			if (type_array.in_array(di)) {
				tek_element.setAttribute("class", "");
			}
			else {
				tek_element.setAttribute("class", "invisible");
			}
		}
	}
}

function OnLoadAction() {
	document.getElementById(list_name+"_0").setAttribute("onChange", "ShowAdditionals(0)");
    ShowAdditionals(0);
}

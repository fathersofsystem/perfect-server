//
//  other.swift
//  perfect-server
//
//  Created by Aleksandr on 18/07/2017.
//
//

import Foundation
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

func createCookie(name: String, value: String, time: Int) -> PerfectHTTP.HTTPCookie {
    let iLoveMyCookie = PerfectHTTP.HTTPCookie(
        name: name,
        value: value,
        expires: .relativeSeconds(time),
        path: "/",
        secure: false,
        httpOnly: false
    )
    return iLoveMyCookie
}

func convertString(_ str: String?) -> String {
    if let c = str {
        return c
    }
    else {
        fatalError("Fatal Error")
    }
}

func convertInt(_ value: Int?) -> Int {
    if let c = value {
        return c
    }
    else {
        fatalError("Fatal Error")
    }
}

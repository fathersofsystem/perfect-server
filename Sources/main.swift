

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import StORM
import MySQLStORM


let server = HTTPServer()
var routes = Routes()
server.serverPort = 8181
server.documentRoot="./webroot"

MySQLConnector.host		= "127.0.0.1"
MySQLConnector.username	= "root"
MySQLConnector.password	= "root"
MySQLConnector.database	= "mydb"
MySQLConnector.port		= 3306

routes.add(method: .post, uri:"/{action}/{subaction}", handler: post_Handler)
routes.add(method: .get, uri:"/{action}/{subaction}", handler: get_Handler)
routes.add(method: .get, uri:"/{action}", handler: get_Handler)
routes.add(method: .get, uri:"/", handler: get_Handler)


let massive = [String: String]()
print(type(of: massive))

server.addRoutes(routes)
do {
    try server.start()
} catch {
    fatalError("\(error)")
}

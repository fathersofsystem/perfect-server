//
//  templates.swift
//  perfect-server
//
//  Created by Aleksandr on 12/07/2017.
//
//

import Foundation
import PerfectHTTP


protocol Templated {
    
    var values:Dictionary<String, Any> {
        get
    }
    var webrootFile:String {
        get
    }
}

class Template:Templated {
    
    var values: Dictionary<String, Any> = [:]
    var webrootFile: String = ""
    
    func makePost(action: String, subaction: String, flag: Bool, params: Array<(String, String)>) {
        self.values = MustacheEvaluationContext.MapType()
        self.values["repo"] = true
        let userHelper = UserHelper()
        if(action == "registration" && subaction == "complete" && flag == true) {
            let user = userHelper.getUser(login: params[0].1)
            self.values["title"] = "Поздравляем с успешной регистрацией"
            self.values["main"] = "Поздравляем с успешной регистрацией"
            self.values["name"] = user.firstname
            self.webrootFile = "index.html"
        }
        else if(action == "create_protocol" && subaction == "complete") {
            let user = userHelper.getUser(login: params[0].1)
            self.values["title"] = "Протокол успешно отправлен"
            self.values["main"] = "Протокол успешно отправлен"
            self.values["name"] = "Александр"
            self.webrootFile = "index.html"
        }
        else if(action == "registration" && subaction == "complete" && flag == false) {
            self.values["title"] = "Регистрация"
            self.webrootFile = "registration.html"
            
        }
        else if(action == "enter" && subaction == "complete" && flag == true) {
            let user = userHelper.getUser(login: params[0].1)
            self.values["title"] = "Успешный вход"
            self.values["main"] = "Вы успешно вошли"
            self.values["name"] = user.firstname
            self.webrootFile = "index.html"
        }
        else if(action == "enter" && subaction == "complete" && flag == false) {
            self.values["title"] = "Вход"
            self.webrootFile = "login.html"
        }
    }
    
    func make(action: String, flag: Bool, cookies: Array<(String, String)>) {
        let userHelper = UserHelper()
        self.values = MustacheEvaluationContext.MapType()
        
        if(flag == true){
            let user = userHelper.getUser(login: cookies[0].1)
            self.values["repo"] = true
            self.values["name"] = user.firstname
        }
        if(action == "exit") {
            self.webrootFile = "exit.html"
        }
        if(action == "developers") {
            self.values["title"] = "О разработчиках"
            self.webrootFile = "developers.html"
        }
        else if(action == "personal" && flag == true) {
            let user = userHelper.getUser(login: cookies[0].1)
            self.values["title"] = "Личный кабинет"
            self.values["name"] = user.firstname
            self.values["lastname"] = user.lastname
            self.webrootFile = "personal_area.html"
        }
        else if(action == "personal" && flag == false) {
            self.values["title"] = "Нету доступа"
            self.webrootFile = "access.html"
        }
        else if(action == "devices" && flag == true) {
            self.values["title"] = "Девайсы"
            self.webrootFile = "devices.html"
        }
        else if(action == "devices" && flag == false) {
            self.values["title"] = "Нет доступа"
            self.webrootFile = "access.html"
        }
        else if(action == "login") {
            self.values["title"] = "Вход"
            self.webrootFile = "login.html"
        }
        else if(action == "feed") {
            self.values["title"] = "Новости"
            self.values["main"] = "На данный момент нет новостей."
            self.webrootFile = "feed.html"
        }
        else if(action == "registration") {
            self.values["title"] = "Регистрация"
            self.webrootFile = "registration.html"
        }
        else if(action == "create_protocol" && flag == true) {
            self.values["title"] = "Создать протокол"
            self.webrootFile = "new_protocol.html"
        }
        else if(action == "create_protocol" && flag == false) {
            self.values["title"] = "Нет доступа"
            self.webrootFile = "access.html"
        }
        else if(action == "main") {
            self.values["title"] = "Главная страница"
            self.values["main"] = "Добро пожаловать на официальный сайт FutureLab"
            self.webrootFile = "index.html"
        }
    }
}

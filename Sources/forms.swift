//
//  forms.swift
//  perfect-server
//
//  Created by Aleksandr on 19/07/2017.
//
//

import Foundation
import PerfectHTTP


class Typed {
    var firstPosition: String
    var name: String
    
    init(name: String,firstPosition: String) {
        self.firstPosition = firstPosition
        self.name = name
    }
}

class DensitySolution: Typed {
    var lengthWave: String
    

    
    init(name: String, firstPosition: String, lengthWave: String) {
        self.lengthWave = lengthWave
        super.init(name: name, firstPosition: firstPosition)
    }
}

class ProcessingResult: Typed {
    var concetration: String

    
    init(name: String, firstPosition: String, concetration: String) {
        self.concetration = concetration
        super.init(name: name, firstPosition: firstPosition)
    }
}

class MovePlate: Typed {
    var lastPosition: String
    
    init(name: String, firstPosition: String, lastPosition: String) {
        self.lastPosition = lastPosition
        super.init(name: name, firstPosition: firstPosition)
    }
}

class Incubation: Typed {
    var temperature: String
    var time: String
    var speed: String
    init(name: String, firstPosition: String, temperature: String, time: String, speed: String) {
        self.temperature = temperature
        self.time = time
        self.speed = speed
        super.init(name: name, firstPosition: firstPosition)
    }
}

class Dosing: Typed {
    var volume: String
    var lastPosition: String
    var firstRow: String
    var numbersChannels: String
    
    init(name: String, firstPosition: String, volume: String, lastPosition: String, firstRow: String,
        numbersChannels: String) {
        self.volume = volume
        self.lastPosition = lastPosition
        self.firstRow = firstRow
        self.numbersChannels = numbersChannels
        super.init(name: name, firstPosition: firstPosition)
    }
}

class Washing: Typed {
    var volume: String
    var temperature: String
    var speed: String
    var multiplicity: String
    var positionWater: String
    init(name: String, firstPosition: String, volume: String, temperature: String, speed: String, multiplicity: String,   positionWater: String) {
        self.volume = volume
        self.temperature = temperature
        self.speed = speed
        self.multiplicity = multiplicity
        self.positionWater = positionWater
        super.init(name: name, firstPosition: firstPosition)
    }
}

class FormsHelper {
    var dictionary = [String: String]()
    var massive = [Typed]()
    
    init(params: Array<(String, String)>) {
        for(key, value) in params {
            dictionary[key] = value
        }
    }
    func makeObjects() -> Array<(Typed)> {
        let s = Int(convertString(dictionary["obj_instances[0]"]))
        let size = convertInt(s)
        for i in 0...size {
            let str = convertString(dictionary["auth_method[\(i)]"])
            if str == "4" {
                let incubation = Incubation(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), temperature: convertString(dictionary["temp[\(i)]"]) , time: convertString(dictionary["time[\(i)]"]), speed: convertString(dictionary["oborot[\(i)]"]))
            massive.append(incubation)
            }
            else if str == "2" {
                let dosing = Dosing(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), volume: convertString(dictionary["password[\(i)]"]), lastPosition: convertString(dictionary["dest_id[\(i)]"]), firstRow: convertString(dictionary["src_row[\(i)]"]), numbersChannels: convertString(dictionary["kanals[\(i)]"]))
                massive.append(dosing)
            }
            else if str == "1" {
                let movePlate = MovePlate(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), lastPosition: convertString(dictionary["dest_id[\(i)]"]))
                massive.append(movePlate)
            }
            else if str == "5" {
                let processingResult = ProcessingResult(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), concetration: convertString(dictionary["konc[\(i)]"]))
                massive.append(processingResult)
            }
            else if str == "6" {
                let washing = Washing(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), volume: convertString(dictionary["password[\(i)]"]), temperature: convertString(dictionary["time[\(i)]"]), speed: convertString(dictionary["oborot[\(i)]"]), multiplicity: convertString(dictionary["krat[\(i)]"]), positionWater: convertString(dictionary["van[\(i)]"]))
                massive.append(washing)
            }
            else if str == "3" {
                let densitySolution = DensitySolution(name: str, firstPosition: convertString(dictionary["src_id[\(i)]"]), lengthWave: convertString(dictionary["length[\(i)]"]))
                massive.append(densitySolution)
            }
        }
        return massive
    }
    
}











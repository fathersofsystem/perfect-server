//
//  command.swift
//  iot-server
//
//  Created by Daniil on 10/07/2017.
//
//

import Foundation
import PerfectHTTP

enum commandStatus {
    case CMD_OK
    case CMD_INVALID_INPUT
    case CMD_ERROR
}

enum commandError: Error {
    case noObjectinMap(object: String)
}

class command {
    var action:String
    let subaction:String
    var flag: Bool
    var cookies: Array<(String, String)>

    init(action:String,_ subaction:String, flag: Bool, cookies: Array<(String, String)>) throws {
        self.subaction = subaction
        self.action = action
        self.flag = flag
        self.cookies = cookies
        
    }
    func makeViewTemp(template: Template){
        fatalError("View function not set!")

    }
    func makeViewPost(template: Template){
        fatalError("View function not set!")
        
    }
}

class pageCommand:command {
    init(action:String, flag: Bool, cookies: Array<(String, String)>) throws {
        try super.init(action:action, "page", flag: flag, cookies: cookies)
    }
    	override func makeViewTemp(template: Template) {
            template.make(action: action, flag: flag, cookies: cookies)
    }
}

class PostCommand {
    let action: String
    let subaction: String
    let flag: Bool
    let params: Array<(String, String)>
    
    init(action: String, subaction: String, flag: Bool, params: Array<(String, String)>) {
        self.action = action
        self.subaction = subaction
        self.flag = flag
        self.params = params
    }
    
    func makeView(template: Template) {
        template.makePost(action: action, subaction: subaction, flag: flag, params: params)
    }
}

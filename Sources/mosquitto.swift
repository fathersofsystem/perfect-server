//
//  mosquitto.swift
//  perfect-server
//
//  Created by Aleksandr on 18/07/2017.
//
//

import Foundation
import PerfectLib
import PerfectMosquitto

class MosquittoHelper {
    private let mosquitto: Mosquitto
    
    init() {
        self.mosquitto = Mosquitto()
        do {
        try mosquitto.connect(host: "192.168.8.1", port: 1883)
        } catch {
            fatalError("Not connection MQTT to devices")
        }
    }
    
    func sendMessage(topic: String, message: String) -> Bool {
        var msg = Mosquitto.Message()
        msg.topic = topic
        msg.string = message
        do {
            try mosquitto.publish(message: msg)
        } catch {
            return false
        }
        return true
    }
    
    func subscribeTopic(topic: String) {
        do {
        try mosquitto.subscribe(topic: topic)
        try mosquitto.start()
        } catch {
            fatalError("Mosquitto can't start")
        }
        mosquitto.OnMessage = { msg in
            print("\(msg.topic): " + msg.string!)
        }
    }
    ///12d
}

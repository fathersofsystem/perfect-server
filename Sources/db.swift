




import StORM
import MySQLStORM
import CryptoSwift

class User: MySQLStORM {
    
    var id: Int = 0
    var login: String = ""
    var password: String = ""
    var firstname: String = ""
    var lastname: String = ""
    
    override open func table() -> String { return "users" }
    
    override func to(_ this: StORMRow) {
        id = this.data["id"] as? Int ?? 0
        login = this.data["login"] as? String ?? ""
        password = this.data["password"] as? String ?? ""
        firstname = this.data["firstname"] as? String ?? ""
        lastname  = this.data["lastname"] as? String ?? ""
    }
    
    func rows() -> [User] {
        var rows = [User]()
        print(results.rows.count)
        for i in 0..<self.results.rows.count {
            let row = User()
            row.to(self.results.rows[i])
            rows.append(row)
        }
        return rows
    }
}

class UserHelper {
    
    func add(login: String, password: String, firstname: String, lastname: String) -> Bool {
        let user = User()
        user.login = login
        user.password = password.sha1()
        user.firstname = firstname
        user.lastname = lastname
        do {
        try user.create()
        return true
        } catch {
            return false
        }
    }
    
    func selectUser(login: String, password: String) -> Bool {
        let user = User()
        do {
        try user.find([("login", login)])
        } catch {
            return false
        }
        if(user.password == password){
            return true
        }
        return false
    }
    
    func getUser(login: String) -> User {
        let user = User()
        do {
            try user.find([("login", login)])
        } catch {
            return user
        }
        return user
    }
    
}








//
//  handlers.swift
//  perfect-server
//
//  Created by Aleksandr on 14/07/2017.
//
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer


struct mustacheHelper: MustachePageHandler {
    var values:  MustacheEvaluationContext.MapType
    func extendValuesForResponse(context contxt: MustacheWebEvaluationContext, collector: MustacheEvaluationOutputCollector) {
        contxt.extendValues(with: values)
        do {
            try contxt.requestCompleted(withCollector: collector)
        } catch {
            let response = contxt.webResponse
            response.appendBody(string: "\(error)")
            response.completed()
        }
    }
}

func get_Handler(request: HTTPRequest, response: HTTPResponse) {
    let action = request.urlVariables["action"] ?? "mainpage"
    let subaction = request.urlVariables["subaction"] ?? "main"
    if((action != "styles" && subaction != "styles") && subaction != "main") {
    print("GET \(action)/\(subaction)")
    }
    else if(action != "styles") {
        print("GET \(action)")
    }
    let template = Template()
    var flag: Bool = false
    var commandObj:command
    let userHelper = UserHelper()
    let cookies = request.cookies
    if(cookies.count == 2) {
        let fl = userHelper.selectUser(login: request.cookies[0].1 , password: request.cookies[1].1)
        if(fl == true) {
            flag = true
        }
    }
    else {
        flag = false
    }
    do {
        //choosing right command
        if(action == "developers") {
            commandObj = try pageCommand(action: "developers", flag: flag, cookies: cookies)
        }
        else if(action == "create_protocol") {
            commandObj = try pageCommand(action: "create_protocol", flag: flag, cookies: cookies)
        }
        else if(action == "feed") {
            commandObj = try pageCommand(action: "feed", flag: flag, cookies: cookies)
        }
        else if(action == "devices") {
            commandObj = try pageCommand(action: "devices", flag: flag, cookies: cookies)
        }
        else if(action == "personal") {
            commandObj = try pageCommand(action: "personal", flag: flag, cookies: cookies)
        }
        else if(action == "login") {
            commandObj = try pageCommand(action: "login", flag: flag, cookies: cookies)
        }
        else if(action == "registration") {
            commandObj = try pageCommand(action: "registration", flag: flag, cookies: cookies)
        }
        else if (action=="mainpage" || action == "index") {
            commandObj = try pageCommand(action:"main", flag: flag, cookies: cookies)
        }
        else if(action == "exit") {
            let loginCookie = createCookie(name: "PerfectLogin", value: "", time: 3600)
            let passwordCookie = createCookie(name: "PerfectLogin", value: "", time: 3600)
            response.addCookie(loginCookie)
            response.addCookie(passwordCookie)
            commandObj = try pageCommand(action: "exit", flag: flag, cookies: cookies)
        }
        else {
            switch subaction {
                case " ":
                    commandObj = try pageCommand(action: action, flag: flag, cookies: cookies)
            default:
                // serving static content
                // Initialize the StaticFileHandler with a documentRoot
                let handler = StaticFileHandler(documentRoot: "./webroot")
                
                // trigger the handling of the request,
                // with our documentRoot and modified path set
                handler.handleRequest(request: request, response: response)
                return
            }
        }
    }
    catch commandError.noObjectinMap(let object) {
        fatalError("No object in map with "+object+" name")
    }
    catch {
        fatalError("Something strange just happened")
    }
    commandObj.makeViewTemp(template: template)
    mustacheRequest(request: request, response: response, handler: mustacheHelper(values: template.values),
                    templatePath:"\(request.documentRoot)/\(template.webrootFile)")
}



    func post_Handler(request: HTTPRequest, response: HTTPResponse) {
        let action = request.urlVariables["action"] ?? "mainpage"
        let subaction = request.urlVariables["subaction"] ?? "main"
        let template = Template()
        print("POST \(action)/\(subaction)")
        let userHelper = UserHelper()
        var commandObj: PostCommand
        let params = request.postParams
        if(action == "registration" && subaction == "complete") {
            let flag = userHelper.add(login: params[0].1, password: params[3].1, firstname: params[1].1, lastname: params[2].1)
            if(flag == true) {
                let loginCookie = createCookie(name: "PerfectLogin", value: params[0].1, time: 3600)
                let passwordCookie = createCookie(name: "PerfectPassword", value: params[3].1.sha1(), time: 3600)
                response.addCookie(loginCookie)
                response.addCookie(passwordCookie)
            }
            commandObj = PostCommand(action: action, subaction: subaction, flag: flag, params: params)
            commandObj.makeView(template: template)
        }
        else if(action == "create_protocol" && subaction == "complete") {
            let formsHelper = FormsHelper(params: params)
            let m = formsHelper.makeObjects()
            commandObj = PostCommand(action: action, subaction: subaction, flag: true, params: params)
            commandObj.makeView(template: template)
            //let mosqHelper = MosquittoHelper()
            
        }
        else if(action == "enter" && subaction == "complete") {
            let flag = userHelper.selectUser(login: params[0].1, password: params[1].1.sha1())
            if(flag == true) {
                let loginCookie = createCookie(name: "PerfectLogin", value: params[0].1, time: 3600)
                let passwordCookie = createCookie(name: "PerfectPassword", value: params[1].1.sha1(), time: 3600)
                response.addCookie(loginCookie)
                response.addCookie(passwordCookie)
            }
            commandObj = PostCommand(action: action, subaction: subaction, flag: flag, params: params)
            commandObj.makeView(template: template)

        }
        mustacheRequest(request: request, response: response, handler: mustacheHelper(values: template.values),
                        templatePath:"\(request.documentRoot)/\(template.webrootFile)")
    }









